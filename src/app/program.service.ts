import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { SettingsService } from './settings.service';
import { ProgramServiceResponse } from './api/response/programServiceResponse';

@Injectable()
export class ProgramService {
  constructor(private http: HttpClient, private settingsService: SettingsService) {}

  /**
   * HTTP GET
   */
  public getServiceStatus(): Observable<ProgramServiceResponse> {
    const url = this.settingsService.getUrlForServiceAndPath('program', 'status');
    console.log(`getProgramServiceStatus: url = ${url}`);
    return this.http.get<ProgramServiceResponse>(url.toString());
  }

  /**
   * HTTP GET
   */
  public startService(setpoint: number): Observable<ProgramServiceResponse> {
    const url = this.settingsService.getUrlForServiceAndPath('program', 'start');
    url.searchParams.append('setpoint', setpoint.toString());
    console.log(`startService: url = ${url}`);
    return this.http.get<ProgramServiceResponse>(url.toString());
  }

  /**
   * HTTP GET
   */
  public pauseService(): Observable<ProgramServiceResponse> {
    const url = this.settingsService.getUrlForServiceAndPath('program', 'pause');
    console.log(`pauseService: url = ${url}`);
    return this.http.get<ProgramServiceResponse>(url.toString());
  }

  /**
   * HTTP GET
   */
  public updateService(setpoint: number): Observable<ProgramServiceResponse> {
    const url = this.settingsService.getUrlForServiceAndPath('program', 'update');
    url.searchParams.append('setpoint', setpoint.toString());
    console.log(`updateService: url = ${url}`);
    return this.http.get<ProgramServiceResponse>(url.toString());
  }

  /**
   * HTTP GET
   */
  public resumeService(): Observable<ProgramServiceResponse> {
    const url = this.settingsService.getUrlForServiceAndPath('program', 'resume');
    console.log(`resumeService: url = ${url}`);
    return this.http.get<ProgramServiceResponse>(url.toString());
  }

  /**
   * HTTP GET
   */
  public stopService(): Observable<ProgramServiceResponse> {
    const url = this.settingsService.getUrlForServiceAndPath('program', 'stop');
    console.log(`stopService: url = ${url}`);
    return this.http.get<ProgramServiceResponse>(url.toString());
  }
}
