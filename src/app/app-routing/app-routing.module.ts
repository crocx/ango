import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { Routes } from '@angular/router/src/config';

import { TemperatureComponent } from '../temperature/temperature.component';
import { SettingsComponent } from '../settings/settings.component';
import { AboutComponent } from '../about/about.component';

const routes: Routes = [
  { path: '', component: TemperatureComponent },
  { path: 'temperature', component: TemperatureComponent },
  { path: 'settings', component: SettingsComponent },
  { path: 'about', component: AboutComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {}
