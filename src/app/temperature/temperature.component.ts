import { Component, OnInit, OnDestroy, OnChanges, SimpleChanges, Input, EventEmitter } from '@angular/core';
import { Subscription } from 'rxjs/Subscription';
import { TimerObservable } from 'rxjs/observable/TimerObservable';
import { SensorsService } from '../sensors.service';
import { ServiceStatusResponse } from '../api/response/serviceStatusResponse';
import { ServiceStatus } from '../api/serviceStatus';
import { SettingsService } from '../settings.service';
import { ProgramService } from '../program.service';
import { MatSliderChange } from '@angular/material';

@Component({
  selector: 'app-temperature',
  templateUrl: './temperature.component.html',
  styleUrls: ['./temperature.component.scss']
})
export class TemperatureComponent implements OnInit, OnDestroy, OnChanges {
  public currentTemperature = '??';
  public currentServiceTemperature: string;
  public lastRefreshed: Date;
  public responseMessage: string;
  public messageColorClass: string;
  public showRefreshingSpinner = false;

  public disableButtonStart = true;
  public disableButtonGetTemperature = true;

  public showButtonStart = true;
  public showButtonPause = false;
  public showButtonResume = false;
  public showButtonUpdate = false;
  public showButtonStop = false;
  public showButtonGetTemperature = false;

  private timerSub: Subscription;
  private _isServiceRunning = false;
  private lastTemperatureResponseOk = false;

  get isServiceRunning(): boolean {
    return this._isServiceRunning;
  }

  get setpoint(): number {
    return this.settingsService.sensorData.controlData.setpoint;
  }

  constructor(
    public sensorsService: SensorsService,
    public settingsService: SettingsService,
    private programService: ProgramService
  ) {}

  ngOnInit() {
    this.showButtonGetTemperature = false;
    this.getProgramServiceStatus();
    this.startUpdateTimer();

    // console.log(`programService.getStatus(): status = ${JSON.stringify(this.programService.getServiceStatus())}`);
  }

  ngOnChanges(changes: SimpleChanges): void {
    console.log(`ngOnChanges(): changes = ${JSON.stringify(changes)}`);
  }

  ngOnDestroy(): void {
    this.stopUpdateTimer();
  }

  public getSensorServiceStatus(ok: (res: ServiceStatusResponse) => void = null) {
    this.sensorsService.getServiceStatus().subscribe(
      res => {
        console.log(`getSensorServiceStatus(): data = ${JSON.stringify(res)}`);
        // this._isServiceRunning = res.status === ServiceStatus.RUNNING;
        // this.handleResponse('status', res);

        // this.disableButtonStart = false;
        this.disableButtonStart = this.setpoint === undefined;
        if (ok !== null) {
          ok(res);
        }
      },
      err => {
        // this.handleErrorResponse('status', err);
        this.disableButtonStart = true;
      },
      () => {}
    );
  }

  public getTemperature() {
    this.sensorsService.getTemperature().subscribe(
      res => {
        // Read the result field from the JSON response.
        // this.handleResponse('get temperature', res);
        // console.log(`getTemperature(): data = ${JSON.stringify(res)}`);

        this.currentTemperature = res.data.data.toString();
        this.currentServiceTemperature = this.currentTemperature;
        this.lastRefreshed = res.time;

        // check service status (to update the UI and stuff) if we didn't get any response before (but did now)
        if (!this.lastTemperatureResponseOk) {
          this.getSensorServiceStatus();
          this.getProgramServiceStatus();
        }
        this.lastTemperatureResponseOk = true;
      },
      err => {
        this.lastRefreshed = new Date();

        // check service status (to update the UI and stuff) if we did get a response before (but didn't now)
        if (!this.lastTemperatureResponseOk) {
          return;
        }
        this.lastTemperatureResponseOk = false;

        this.currentTemperature = 'N/A';
        this.currentServiceTemperature = undefined;
        this.showRefreshingSpinner = false;
        this.handleErrorResponse('get temperature', err);

        this.getSensorServiceStatus();
        this.getProgramServiceStatus();
      },
      () => {
        this.showRefreshingSpinner = false;
        // this.disableButtonGetTemperature = this.isServiceRunning;
        this.updateUi();
      }
    );

    this.showRefreshingSpinner = true;
    // this.disableButtonGetTemperature = true;
  }

  public getProgramServiceStatus(ok: (res: ServiceStatusResponse) => void = null) {
    this.programService.getServiceStatus().subscribe(
      res => {
        console.log(`getProgramServiceStatus(): data = ${JSON.stringify(res)}`);
        // this._isServiceRunning = res.status === ServiceStatus.RUNNING;
        this.handleResponse('status', res);
        if (ok !== null) {
          ok(res);
        }
      },
      err => {
        this.handleErrorResponse('status', err);
      },
      () => {}
    );
  }

  public startProgramService() {
    console.log(`startProgramService(): setpoint = ${this.setpoint}`);

    this.programService.startService(this.setpoint).subscribe(
      res => {
        this.handleResponse('start', res);
      },
      err => {
        this.handleErrorResponse('start', err);
        this.showRefreshingSpinner = false;
      },
      () => {
        console.log(`startProgramService(): service started`);
      }
    );
  }

  public pauseProgramService() {
    console.log(`pauseProgramService()`);

    this.programService
      .pauseService()
      .subscribe(res => this.handleResponse('pause', res), err => this.handleErrorResponse('pause', err));
  }

  public resumeProgramService() {
    console.log(`resumeProgramService()`);

    this.programService
      .resumeService()
      .subscribe(res => this.handleResponse('resume', res), err => this.handleErrorResponse('resume', err));
  }

  public updateProgramService() {
    console.log(`updateProgramService(): setpoint = ${this.setpoint}`);

    this.programService
      .updateService(this.setpoint)
      .subscribe(res => this.handleResponse('update', res), err => this.handleErrorResponse('update', err));
  }

  public stopProgramService() {
    console.log(`stopProgramService()`);

    this.programService.stopService().subscribe(
      res => this.handleResponse('stop', res),
      err => this.handleErrorResponse('stop', err),
      () => {
        // this.currentServiceTemperature = undefined;
        this.showRefreshingSpinner = false;

        console.log(`stopProgramService(): service stopped`);
      }
    );
  }

  private handleResponse(operation: string, res: ServiceStatusResponse) {
    const msg = res.message;
    console.log(`Program service: ${operation.toUpperCase()} = ${res.status} (${res.errorCode})`);
    this.responseMessage = msg;
    this.messageColorClass = 'temperature service message ok';

    this.updateUi(res);
  }

  private handleErrorResponse(operation: string, err: any) {
    console.log(`Error response: ${JSON.stringify(err)}`);
    // let msg = err ? (err.error ? err.error.message || err.message : err) : err;
    const msg = `${err.message}\nInternal error:\n${err.error.message},\ncode = ${err.error.errorCode}`;
    console.log(`Program service error: ${operation.toUpperCase()} = ${JSON.stringify(err)}`);
    this.responseMessage = msg;
    this.messageColorClass = 'temperature service message error';

    this.updateUi(err.error);
  }

  private startUpdateTimer() {
    if (this.timerSub == null || this.timerSub.closed) {
      this.timerSub = TimerObservable.create(1000, this.settingsService.sensorData.controlData.refreshTime).subscribe(
        t => this.getTemperature()
      );
    }

    this.updateUi();
  }

  private stopUpdateTimer() {
    if (this.timerSub) {
      this.timerSub.unsubscribe();

      const msg = `Update timer stopped: timerSub.closed = ${this.timerSub.closed}`;
      this.responseMessage += `\n</br>${msg}`;
      console.log(msg);

      this.timerSub = null;
    }

    this.updateUi();
  }

  // private checkUpdateTimer() {
  //   if (this.isServiceRunning) {
  //     this.startUpdateTimer();
  //   } else {
  //     this.stopUpdateTimer();
  //   }
  // }

  private updateUi(response: ServiceStatusResponse = null) {
    this.disableButtonGetTemperature = this.timerSub != null;

    if (response == null) {
      return;
    }

    // console.log(
    //   `updateUi(): ${response.errorCode}, ${BaseResponseCodes[BaseResponseCodes.OK]}, ${
    //     BaseResponseCodes[response.errorCode]
    //   }, ${BaseResponseCodes.OK}`
    // );

    this.showButtonStart = false;
    this.showButtonPause = false;
    this.showButtonResume = false;
    this.showButtonUpdate = false;
    this.showButtonStop = false;

    if (response.status === undefined) {
      this.showButtonStart = true;
      return;
    }

    switch (ServiceStatus[response.status.toString()]) {
      case ServiceStatus.RUNNING:
        this.showButtonPause = true;
        this.showButtonUpdate = true;
        this.showButtonStop = true;
        this._isServiceRunning = true;

        // this.disableButtonGetTemperature = this.isServiceRunning;

        // TODO: get this setpoint from AppSettings
        // this.settingsService.sensorData.controlData.setpoint = response.pidData.setpoint;
        break;

      case ServiceStatus.PAUSED:
        this.showButtonResume = true;
        this.showButtonUpdate = true;
        this.showButtonStop = true;
        this._isServiceRunning = true;
        break;

      default:
        this.showButtonStart = true;
        this._isServiceRunning = false;

        // this.disableButtonGetTemperature = false;
        break;
    }
  }

  public onSliderValueChange(event: MatSliderChange) {
    // console.log(`onSliderValueChange(): event: value = ${event.value}`);
    this.disableButtonStart = this.setpoint === undefined;
  }
}
