import { BaseStatusDataResponse } from './baseStatusDataResponse';
import { ProgramData } from '../data/programData';

export class ProgramServiceResponse extends BaseStatusDataResponse<ProgramData> {}
