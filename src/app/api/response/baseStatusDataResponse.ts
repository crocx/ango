import { ServiceStatusResponse } from './serviceStatusResponse';

/**
 * See package com.labo.kotu.web.response (server side) for reference.
 */
export abstract class BaseStatusDataResponse<T> extends ServiceStatusResponse {
  data?: T = null;
}
