/**
 * See com.labo.kotu.web.response (server side) for reference.
 */
export enum BaseResponseCodes {
  OK = 0,

  /*
   * General errors 1 - 99
   */

  ERROR = 1,

  /*
   * Service errors 100 - 999
   */

  ERROR_SERVICE_ALREADY_STARTED = 110,
  ERROR_SERVICE_NOT_YET_STARTED = 120,

  /*
   * Hardware errors 1000 - 1999
   */

  ERROR_HW_PIN_PROVISIONING_FAILED = 1110
}
