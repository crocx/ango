/**
 * See package com.labo.kotu.web.response (server side) for reference.
 */
export class BaseResponse {
  time: Date;
  message?: string;
  /** XXX: Actually type of BaseResponseCodes, but parsing not yet working properly. */
  errorCode: string;
  // errorCode: BaseResponseCodes;
}
