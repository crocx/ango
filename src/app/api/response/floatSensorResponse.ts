import { BaseData } from '../data/baseData';
import { ServiceStatusResponse } from './serviceStatusResponse';

export class FloatSensorResponse extends ServiceStatusResponse {
  data: BaseData;
}
