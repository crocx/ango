import { BaseResponse } from './baseResponse';
import { ServiceStatus } from '../serviceStatus';

/**
 * See package com.labo.kotu.web.response (server side) for reference.
 */
export class ServiceStatusResponse extends BaseResponse {
  status: ServiceStatus;
}
