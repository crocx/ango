import { BaseStatusDataResponse } from './baseStatusDataResponse';
import { AppSettingsData } from '../data/appSettingsData';

/**
 * See package com.labo.kotu.web.response (server side) for reference.
 */
export class AppSettingsStatusResponse extends BaseStatusDataResponse<AppSettingsData> {}
