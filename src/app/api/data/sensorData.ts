import { ControlData } from './controlData';

export class SensorData {
  public sensorId: string;
  public controlData: ControlData;

  constructor(sensorId?: string, controlData?: ControlData) {
    this.sensorId = sensorId;
    this.controlData = controlData ? controlData : new ControlData();
  }
}
