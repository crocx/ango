import { SensorErrorCode } from '../errorCode';

export class BaseData {
  sensorId: string;
  data: number;
  time: Date;
  cached: boolean;
  errorCode: SensorErrorCode;

  public toJson(): string {
    return JSON.stringify(this);
  }
}
