export class FloatSensorData {
  sensorId: string;
  data: number;
}
