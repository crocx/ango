export class AppSettingsData {
  public sensorId: string;
  public gpio = 3;
  // public kP = 1;
  // public kI = 0.1;
  // public kD = 0.1;
  public kp = 1;
  public ki = 0.1;
  public kd = 0.1;
  public diffTolerance = 0.1;
  public refreshTime = 1000;
}
