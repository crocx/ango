import { BaseData } from './baseData';

export class ProgramData extends BaseData {
  setpoint: number;
}
