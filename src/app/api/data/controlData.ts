export class ControlData {
  public gpio?;
  public setpoint = 22.2;
  public kP?;
  public kI?;
  public kD?;
  public diffTolerance?;
  public refreshTime?;
  // public gpio = 3;
  // public setpoint = 40;
  // public kP = 1;
  // public kI = 0.1;
  // public kD = 0.1;
  // public diffTolerance = 0.1;
  // public refreshTime = 1000;
}
