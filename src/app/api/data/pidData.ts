export class PidData {
  setpoint: number;
  kP: number;
  kI: number;
  kD: number;
  kF: number;
}
