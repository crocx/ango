/**
 * See com.labo.kotu.hw.data (server side) for reference.
 */
export enum SensorErrorCode {
  OK,
  GENERAL_EXCEPTION,
  CANNOT_READ_SENSOR_VALUE,
  CRC_FALSE
}
