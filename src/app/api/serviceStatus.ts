/**
 * See com.labo.kotu.main.service (server side) for reference.
 */
export enum ServiceStatus {
  NOT_STARTED,
  RUNNING,
  PAUSED,
  ENDED
}
