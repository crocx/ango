import { HttpClient } from '@angular/common/http';
import { Injectable, OnInit, OnChanges, SimpleChanges } from '@angular/core';
import { SensorData } from './api/data/sensorData';
import { AppSettingsStatusResponse } from './api/response/appSettingsStatusResponse';
import { Observable } from 'rxjs/Observable';

@Injectable()
export class SettingsService implements OnInit, OnChanges {
  // public static readonly DEFAULT_API_SERVER_BASE_URL = '192.168.0.162:3000';
  public static readonly DEFAULT_API_SERVER_BASE_URL = 'localhost:3000';

  public serviceIP = SettingsService.DEFAULT_API_SERVER_BASE_URL;

  private _sensorData: SensorData;

  get sensorData(): SensorData {
    return this._sensorData;
  }

  constructor(private http: HttpClient) {
    this._sensorData = new SensorData();
    console.log(`SettingsService.constructor(): _sensorData = ${JSON.stringify(this._sensorData)}`);
  }

  ngOnInit() {
    console.log(`SettingsService.ngOnInit()`);
    this.loadAppSettings();
  }

  ngOnChanges(changes: SimpleChanges): void {
    console.log(`SettingsService.ngOnChanges(): changes = ${JSON.stringify(changes)}`);
  }

  public getAppSettings(): Observable<AppSettingsStatusResponse> {
    const url = this.getUrlForServiceAndPath('appsettings', 'status/data');
    console.log(`getAppSettings(): url = ${url}`);
    return this.http.get<AppSettingsStatusResponse>(url.toString());
  }

  public updateAppSettings(): Observable<AppSettingsStatusResponse> {
    const url = this.getUrlForServiceAndPath('appsettings', 'update');
    if (this._sensorData.sensorId !== undefined) {
      url.searchParams.append('sensorId', this._sensorData.sensorId.toString());
    }
    if (this._sensorData.controlData.gpio !== undefined) {
      url.searchParams.append('gpio', this._sensorData.controlData.gpio.toString());
    }
    if (this._sensorData.controlData.kP !== undefined) {
      url.searchParams.append('kP', this._sensorData.controlData.kP.toString());
    }
    if (this._sensorData.controlData.kI !== undefined) {
      url.searchParams.append('kI', this._sensorData.controlData.kI.toString());
    }
    if (this._sensorData.controlData.kD !== undefined) {
      url.searchParams.append('kD', this._sensorData.controlData.kD.toString());
    }
    if (this._sensorData.controlData.diffTolerance !== undefined) {
      url.searchParams.append('diffTolerance', this._sensorData.controlData.diffTolerance.toString());
    }
    if (this._sensorData.controlData.refreshTime !== undefined) {
      url.searchParams.append('refreshTime', this._sensorData.controlData.refreshTime.toString());
    }
    console.log(`updateAppSettings(): url = ${url}`);
    return this.http.get<AppSettingsStatusResponse>(url.toString());
  }

  public loadAppSettings() {
    console.log(`SettingsService.loadAppSettings()`);

    this.getAppSettings().subscribe(
      res => {
        console.log(`loadAppSettings(): data = ${JSON.stringify(res)}`);
        this._sensorData.sensorId = res.data.sensorId;
        this._sensorData.controlData.diffTolerance = res.data.diffTolerance;
        this._sensorData.controlData.gpio = res.data.gpio;
        this._sensorData.controlData.kP = res.data.kp;
        this._sensorData.controlData.kI = res.data.ki;
        this._sensorData.controlData.kD = res.data.kd;
        this._sensorData.controlData.refreshTime = res.data.refreshTime;
      },
      err => {
        console.log(`loadAppSettings() - error: data = ${JSON.stringify(err)}`);
      },
      () => {
        console.log(`SettingsService.loadAppSettings() - loaded: _sensorData = ${JSON.stringify(this._sensorData)}`);
      }
    );
  }

  public saveAppSettings() {
    console.log(`SettingsService.saveAppSettings()`);

    this.updateAppSettings().subscribe(
      res => {
        console.log(`saveAppSettings(): data = ${JSON.stringify(res)}`);
        this._sensorData.sensorId = res.data.sensorId;
        this._sensorData.controlData.diffTolerance = res.data.diffTolerance;
        this._sensorData.controlData.gpio = res.data.gpio;
        this._sensorData.controlData.kP = res.data.kp;
        this._sensorData.controlData.kI = res.data.ki;
        this._sensorData.controlData.kD = res.data.kd;
        this._sensorData.controlData.refreshTime = res.data.refreshTime;
      },
      err => {
        console.log(`saveAppSettings() - error: data = ${JSON.stringify(err)}`);
      },
      () => {}
    );
  }

  public getUrlForServiceAndPath(service: string, path: string): URL {
    return new URL(`http://${this.serviceIP}/api/v1/${service}/${path}`);
  }
}
