import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SettingsSensorsComponent } from './settings-sensors.component';

describe('SettingsSensorsComponent', () => {
  let component: SettingsSensorsComponent;
  let fixture: ComponentFixture<SettingsSensorsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SettingsSensorsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SettingsSensorsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
