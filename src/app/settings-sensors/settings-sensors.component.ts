import { Component, OnInit, Injectable, Input } from '@angular/core';
import { SensorData } from '../api/data/sensorData';

@Component({
  selector: 'app-settings-sensors',
  templateUrl: './settings-sensors.component.html',
  styleUrls: ['./settings-sensors.component.scss']
})
@Injectable()
export class SettingsSensorsComponent implements OnInit {
  @Input() public sensorData: SensorData;

  constructor() {}

  ngOnInit() {}
}
