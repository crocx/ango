import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SettingsGpioComponent } from './settings-gpio.component';

describe('SettingsGpioComponent', () => {
  let component: SettingsGpioComponent;
  let fixture: ComponentFixture<SettingsGpioComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SettingsGpioComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SettingsGpioComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
