import { Component, OnInit, Input } from '@angular/core';
import { SensorData } from '../api/data/sensorData';
import { FormControl } from '@angular/forms';
import { Observable } from 'rxjs/Observable';
import { startWith, map } from 'rxjs/operators';

@Component({
  selector: 'app-settings-gpio',
  templateUrl: './settings-gpio.component.html',
  styleUrls: ['./settings-gpio.component.scss']
})
export class SettingsGpioComponent implements OnInit {
  @Input() public sensorData: SensorData;

  public gpioFormControl: FormControl = new FormControl();
  public filteredGpioOptions: Observable<string[]>;
  public autocompleteGpioOptions = ['3', '22'];

  constructor() {}

  ngOnInit() {
    this.filteredGpioOptions = this.gpioFormControl.valueChanges.pipe(
      startWith(''),
      map(val => this.filterAutocomplete(this.autocompleteGpioOptions, val))
    );
  }

  private filterAutocomplete(autocompleteOptions: string[], val: string): string[] {
    return autocompleteOptions.filter(option => option.toLowerCase().indexOf(val.toLowerCase()) === 0);
  }
}
