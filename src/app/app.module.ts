import { HttpClientModule } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing/app-routing.module';
import { AppComponent } from './app.component';
import { MaterialUiModule } from './material-ui/material-ui.module';
import { SensorsService } from './sensors.service';
import { TemperatureComponent } from './temperature/temperature.component';
import { MainUiComponent } from './main-ui/main-ui.component';
import { SettingsComponent } from './settings/settings.component';
import { SettingsPidComponent } from './settings-pid/settings-pid.component';
import { SettingsSensorsComponent } from './settings-sensors/settings-sensors.component';
import { SettingsGpioComponent } from './settings-gpio/settings-gpio.component';
import { AboutComponent } from './about/about.component';
import { SettingsService } from './settings.service';
import { ProgramService } from './program.service';

@NgModule({
  declarations: [
    AppComponent,
    TemperatureComponent,
    MainUiComponent,
    SettingsComponent,
    SettingsPidComponent,
    SettingsSensorsComponent,
    SettingsGpioComponent,
    AboutComponent
  ],
  imports: [BrowserModule, HttpClientModule, MaterialUiModule, AppRoutingModule],
  providers: [SettingsService, SensorsService, ProgramService],
  bootstrap: [AppComponent]
})
export class AppModule {}
