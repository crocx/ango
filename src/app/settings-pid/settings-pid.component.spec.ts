import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SettingsPidComponent } from './settings-pid.component';

describe('SettingsPidComponent', () => {
  let component: SettingsPidComponent;
  let fixture: ComponentFixture<SettingsPidComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SettingsPidComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SettingsPidComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
