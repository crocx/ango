import { Component, OnInit, Input } from '@angular/core';
import { SensorData } from '../api/data/sensorData';

@Component({
  selector: 'app-settings-pid',
  templateUrl: './settings-pid.component.html',
  styleUrls: ['./settings-pid.component.scss']
})
export class SettingsPidComponent implements OnInit {
  @Input() public sensorData: SensorData;

  constructor() {}

  ngOnInit() {}
}
