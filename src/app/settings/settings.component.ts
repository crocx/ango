import { Component, OnInit, OnChanges, SimpleChanges, OnDestroy, DoCheck, Input } from '@angular/core';
import { FormControl } from '@angular/forms';
import { Observable } from 'rxjs/Observable';
import { map, startWith } from 'rxjs/operators';
import { SettingsService } from '../settings.service';

@Component({
  selector: 'app-settings',
  templateUrl: './settings.component.html',
  styleUrls: ['./settings.component.scss']
})
export class SettingsComponent implements OnInit, OnChanges, OnDestroy {
  public ipFormControl: FormControl = new FormControl();
  public filteredIpOptions: Observable<string[]>;
  public autocompleteIpOptions = [
    'localhost:3000',
    // 'localhost:3001',
    '192.168.0.162:3000',
    // '192.168.0.162:3001',
    'temppo.hopto.org:3000'
    // 'temppo.hopto.org:3001'
  ];

  constructor(public settingsService: SettingsService) {}

  ngOnInit() {
    // this.temperatureService.setCustomApiServerUrl(this.serviceIP);

    // this.addAutocompleteFilter(this.filteredIpOptions, this.ipFormControl, this.autocompleteIpOptions);
    // this.addAutocompleteFilter(this.filteredGpioOptions, this.gpioFormControl, this.autocompleteGpioOptions);
    this.filteredIpOptions = this.ipFormControl.valueChanges.pipe(
      startWith(''),
      map(val => this.filterAutocomplete(this.autocompleteIpOptions, val))
    );

    this.settingsService.loadAppSettings();
  }

  // ngDoCheck(): void {
  //   console.log(`SettingsComponent.ngDoCheck()`);
  // }

  ngOnChanges(changes: SimpleChanges): void {
    console.log(`SettingsComponent.ngOnChanges(): changes = ${JSON.stringify(changes)}`);
  }

  ngOnDestroy(): void {
    console.log(`SettingsComponent.ngOnDestroy()`);
    // this.settingsService.saveAppSettings();
  }

  public saveAppSettings() {
    this.settingsService.saveAppSettings();
  }

  public loadAppSettings() {
    this.settingsService.loadAppSettings();
  }

  private filterAutocomplete(autocompleteOptions: string[], val: string): string[] {
    return autocompleteOptions.filter(option => option.toLowerCase().indexOf(val.toLowerCase()) === 0);
  }
}
