import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-main-ui',
  templateUrl: './main-ui.component.html',
  styleUrls: ['./main-ui.component.scss']
})
export class MainUiComponent {
  isSideNavTextExpanded = false;

  constructor() {}

  public toggleSideNavIconsAlwaysOn() {
    this.isSideNavTextExpanded = !this.isSideNavTextExpanded;
  }
}
