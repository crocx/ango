import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { SettingsService } from './settings.service';
import { ServiceStatusResponse } from './api/response/serviceStatusResponse';
import { FloatSensorResponse } from './api/response/floatSensorResponse';

@Injectable()
export class SensorsService {
  constructor(private http: HttpClient, private settingsService: SettingsService) {}

  /**
   * HTTP GET
   */
  public getTemperature(): Observable<FloatSensorResponse> {
    const sensorData = this.settingsService.sensorData;
    const url = this.settingsService.getUrlForServiceAndPath('sensor', 'get');

    if (sensorData.sensorId !== undefined && sensorData.sensorId !== null && sensorData.sensorId.trim().length > 0) {
      url.searchParams.append('sensorId', sensorData.sensorId);
    }
    // console.log(`getTemperature: url = ${url}`);
    return this.http.get<FloatSensorResponse>(url.toString());
  }

  /**
   * HTTP GET
   */
  public getServiceStatus(): Observable<ServiceStatusResponse> {
    const url = this.settingsService.getUrlForServiceAndPath('sensor', 'status');
    console.log(`getSensorsServiceStatus: url = ${url}`);
    return this.http.get<ServiceStatusResponse>(url.toString());
  }
}
