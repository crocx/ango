# Ango

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 1.6.0.

# How to set up the project and run it

1. Make sure you have the latest NodeJS (9.+) installed.
2. Clone the project via

   `git clone git@github.com:crocy/ango.git`

3. Go to the project folder and run `npm install`

   If you get errors during install see [Fixing npm permissions](https://docs.npmjs.com/getting-started/fixing-npm-permissions).

4. Run a dev server via `npm start`
5. Navigate your browser to http://localhost:4200/
6. To build a production site run `npm run build-prod` and then start a local web server with `npm run ws-start` (the server is started on port `4201` by default, to change that port run the command as `npm run ws-start -- --port <yourPort>`)

---

# Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

---

# Build

Run `ng build` to build the project. The build artifacts will be stored in the `static/` directory. Use the `-prod` flag for a production build.

- *You can/should copy the static content from `static` dir (after building the project via `ng build` or alike) to `kotu/static` folder to get the user UI served on `localhost:3000`.*

---

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/).

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI README](https://github.com/angular/angular-cli/blob/master/README.md).
